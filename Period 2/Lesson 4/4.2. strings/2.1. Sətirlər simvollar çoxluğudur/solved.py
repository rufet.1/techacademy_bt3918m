# -*- coding: utf-8 -*-

# Tək dırnaqlı sətir
setir1 = 'SƏTİR 1'

# Cüt dırnaqlı sətr
setir2 = "SƏTİR 2"

# Üç dəfə təkrarlanan tək dırnaqlı sətir
setir3 = '''SƏTİR 1'''

# Üç dəfə təkrarlanan cüt dırnaqlı sətir
setir4 = """SƏTİR 1"""

# 1. setir1 dəyişənində olan mətnin uzunluğunu ekranda çap edin
print("1. Length of setir1 string is:", len(setir1))

# 2. setir1 dəyişənində olan mətnin T simvolunu indeksin köməkliyi ilə tapıb çap edin
print("2. setir1[2] =>", setir1[2])

# 3. setir1 dəyişənində olan mətnin R simvolunu əks istiqamətdən indeksin köməkliyi ilə tapıb çap edin
print("3. setir1[-3] =>", setir1[-3])

# 4. setir1 dəyişənində olan mətnin hər bir simvolunu a, b, c, d, e, f və g adlı dəyişənlərə mənimsədin
a, b, c, d, e, f, g = setir1
# printing
print("4. a =>", a, "; b =>", b, "; c =>", c, "; d =>", d, "; e =>", e, "; f =>", f, "; g =>", g)

# 5. a dəyişəni verilmişdir və dəyəri rəqəmdir. Onun tipini ekranda çap edin. Daha sonra b dəyişəni təyin edin və a dəyişəninin dəyərini məcburi sətrə konvertasiya edərək b dəyişəninin tipini ekranda çap edin.
a = 22
print("5. Type of a variable is:", type(a), "; value of a variable is:", a)
b = str(a)
print("5. Type of b variable is:", type(b), "; value of b variable is:", b)