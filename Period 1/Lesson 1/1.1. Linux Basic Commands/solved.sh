# Dəyişən yaradın və ona hal-hazırda olduğunuz cari folderin path-ini (adresini) mənimsədin
CARI_DIREKTORIYA=$(pwd)

# Yaratdığınız dəyişəni echo vasitəsi ilə ekranda göstərin
echo "Cari direktoriya:" ${CARI_DIREKTORIYA}

# istifadəçinin home folderinə keçid edin
cd ~
# və ya sadəcə cd

# keçid etdiyiniz direktoriyanın dəqiq path-ini echo vasitəsi ilə print edin
echo "Biz hal hazırda " $(pwd) " folderindəyik"

# home folderində test_folder yaradın
mkdir $(pwd)/test_folder

# yeni yaratdığınız test_folder-ə keçid edin (nisbi path-dən istifadə edərək)
cd ./test_folder

# içərisində olduğunuz cari folder echo vasitəsi ilə print edin
echo "Biz hal hazırda " $(pwd) " folderindəyik"

# test_folder direktoriyasının içərisində sub_folder adlı alt direktoriya yaradın və bu direktoriyaya keçid edin
mkdir ./sub_folder

# yeni yaratdığınız sub_folder-ə keçid edin (nisbi path-dən istifadə edərək)
cd ./sub_folder

# içərisində olduğunuz cari folder echo vasitəsi ilə print edin
echo "Biz hal hazırda " $(pwd) " folderindəyik"

# yeni yaratdığınız sub_folderin içərisində əvvəli tmp_file_ ilə başlayan sonu sıralı rəqəmlə və .txt extension ilə bitən 100 ədəd fayl yaradın
touch tmp_file_{1..100}.txt

# sonu 2 ilə bitən bütün faylları silin (məsələn: tmp_file_2.txt, tmp_file_12.txt və s.)
rm *2.txt

# nisbi path-dən istifadə edərək home folderə qayıdın
cd ../../

# test_folderin adını dəyişərək test_directory edin
mv test_folder test_directory

# test_folderi içərisindəki fayl və folderlərlə birlikdə silin
rm -rf ./test_folder
